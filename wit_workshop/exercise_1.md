# Exercise 1 - Introduction to the project

1. Fork the project and create a branch `exercise_1` from the `master` branch.
2. Go to `Build -> Pipeline editor` and switch to the `exercise_1` branch.
3. Push `Configure pipeline` button and analyze template pipeline definition.
4. Once you know what is going to happen, press commit [IMPORTANT: make sure you commit to the new branch, not master] 
(a new file called `.gitlab-ci.yml` will be added to the branch).
5. Go to `Build -> Pipelines` and see if the pipeline succeeded.
6. Come back to the pipeline editor. Add `image: python:latest` section to your pipeline configuration.
7. Add `before_script` section that’s going to list versions of python and pip.
8. Remove `sleep` commands, commit the file and see if the pipeline succeeded (check python and pip versions used in each 
job).
9. Merge your branch into the `master` branch.